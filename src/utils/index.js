import config from './config';
import storageData from './storageData';

export {
  config,
  storageData
}