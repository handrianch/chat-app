import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, ScrollView, FlatList } from 'react-native'
import { Icon } from 'react-native-elements'
import ChatItem from './ChatItem'
import axios from 'axios'
import { config, storageData } from '../../utils'

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      test: '',
      chatList: [],
      userId: ''
    };
  }

  async componentDidMount() {
    try {
      const token = await storageData.getKey(config.tokenKey)
      const options = {headers: {Authorization : `Bearer ${token}`}}
      const results = await axios.get(`${config.host}/${config.prefix}/rooms`, options)
      const profile = await axios.get(`${config.host}/${config.prefix}/profile`, options)

      this.setState({chatList: results.data, userId: profile.data.id})
    } catch(e) {
      this.setState({test: JSON.stringify(e)})
    }
  }

  render() {
    return (
      <View>
        <View style={styles.containerHeader}>
          <View style={styles.wrapperHeader}>
            <View style={styles.wrapperTitleHeader}>
              <Text style={styles.titleHeader}>ChatApp</Text>
            </View>
            <View style={styles.wrapperIconNewMessage}>
              <Icon name="message-plus" type="material-community" color="#0e1111" />
            </View>
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <FlatList
              data={this.state.chatList}
              renderItem={({item}) => <ChatItem data={item} componentId={this.props.componentId} userId={this.state.userId}/>}
              keyExtractor={(item) => item.id}
            />
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default Home

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#f3f3f3',
    paddingTop: 15
  },
  containerHeader: {
    height: 60,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: '#11ece1'
  },
  wrapperHeader: {
    flexDirection: 'row',
    height: '100%'
  },
  wrapperTitleHeader: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 60
  },
  wrapperIconNewMessage: {
    flex: 0.5,
    justifyContent: 'center'
  }
})