import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight, TouchableWithoutFeedback } from 'react-native'
import Swipeable from 'react-native-swipeable'
import { Icon } from 'react-native-elements'
import { Navigation } from 'react-native-navigation'
import axios from 'axios'
import {config, storageData} from '../../utils'
import ReactTimeAgo from 'react-time-ago'
import JavascriptTimeAgo from 'javascript-time-ago'
import en from 'javascript-time-ago/locale/en'

JavascriptTimeAgo.locale(en)

class ChatItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      chatPressed: false
    };
  }

  rightBtn = [
    <TouchableHighlight>
      <View style={{marginTop: 16, height: 71, justifyContent: 'center', alignItems: 'flex-start', paddingLeft: 18, backgroundColor:"#ED4337" }}>
        <Icon style={{textAlign: 'center'}} size={40} name="delete-forever" type="material-community" color="#f8f8ff" />
      </View>
    </TouchableHighlight>
  ]

  chatPressedHandler = () => {
    this.setState({chatPressed: !this.state.chatPressed})
  }

 openChatHandler = async () => {
    this.chatPressedHandler()
    const roomId = this.props.data.id
    Navigation.push(this.props.componentId, {
      component: {
        name: "chat.room",
        passProps: {
          roomId: roomId,
          userId: this.props.userId
        }
      }
    })
  }

  render() {
    const {sender, receiver, chats} = this.props.data

    return (
        <Swipeable rightButtons={this.rightBtn}>
          <View>
            <Text>{this.state.test}</Text>
            <TouchableWithoutFeedback onPressIn={this.openChatHandler} onPressOut={this.chatPressedHandler}>
              <View style={[styles.wrapper, this.state.chatPressed ? {backgroundColor: '#f8f8f8'} : {}]}>
                <View style={styles.wrapperImage}>
                  <Image source={require('../../assets/images/anjay.jpg')} style={styles.avatar} />
                </View>
                <View style={styles.wrapperChat}>
                  <View style={styles.wrapperUsername}>
                    <Text style={styles.usernameText}>{this.props.userId == sender.id ? receiver.username : sender.username}</Text>
                  </View>
                  <View style={styles.wrapperLastChat}>
                    <Text numberOfLines={1} ellipsizeMode="clip" style={styles.lastChatText}>
                      {chats[0].chat}
                    </Text>
                  </View>
                </View>
                <View style={styles.timeFormat}>
                  {/*<ReactTimeAgo date={new Date()} locale="en"/>*/}
                  <Text style={styles.textTime}>{chats[0].created_at}</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </Swipeable>
    )
  }
}

export default ChatItem

const styles = StyleSheet.create({
  wrapper: {
    padding: 10,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    borderBottomWidth: 1,
    borderBottomColor: '#11ece1'
  },
  wrapperImage: {
    height: 50,
    width: 50
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 100
  },
  wrapperChat: {
    flex: 1,
    paddingLeft: 5,
  },
  timeFormat:{
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 5
  },
  textTime: {
    fontSize: 10
  },
  wrapperUsername: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  usernameText: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    fontSize: 18,
    color: '#0e1111'
  },
  wrapperLastChat: {
    flex: 1,
    justifyContent: 'flex-start'
  },
  lastChatText: {
    fontSize: 13,
    textTransform: 'capitalize',
    color: '#95a5a6'
  }
})