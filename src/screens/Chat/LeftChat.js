import React, { Component } from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Swipeable from 'react-native-swipeable';

class LeftChat extends Component {

  render() {
    return (
        <View style={styles.wrapperChat}>
            <View style={styles.wrapperTime}>
              <Text style={styles.time}>{this.props.time}</Text>
            </View>
            <View style={styles.wrapperBuble}>
              <View style={styles.leftSection}>
                <View style={styles.wrapperProfileImage}>
                  <Image source={require('../../assets/images/anjay.jpg')} style={styles.profileImage} />
                </View>
              </View>
              <View style={styles.rightSection}>
                <View style={styles.wrapperChatText}>
                  <Text style={styles.chatText}>{this.props.chat}</Text>
                </View>
              </View>
            </View>
        </View>
    )
  }
}

export default LeftChat

const styles = StyleSheet.create({
  wrapperChat: {
    minHeight: 100,
    marginBottom: 5
  },
  wrapperTime: {
    height: 35,
    justifyContent: 'center',
    alignItems: 'center'
  },
  time: {
    fontSize: 10
  },
  profileImage: {
    borderRadius: 100,
    width: 30,
    height: 30
  },
  chatText: {
    padding: 15
  },
  wrapperBuble : {
    flex: 1,
    flexDirection: 'row'
  },
  leftSection: {
    width: 50,
    justifyContent: 'flex-end'
  },
  wrapperProfileImage: {
    alignItems: 'flex-end'
  },
  rightSection: {
    justifyContent: 'flex-end',
    marginLeft: 5
  },
  wrapperChatText: {
    maxWidth: 230,
    backgroundColor: '#ebebeb',
    borderRadius: 10,
    borderBottomLeftRadius: 0
  }
})