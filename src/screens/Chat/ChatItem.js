import React, { Component } from 'react'
import { View, Text } from 'react-native'
import LeftChat from './LeftChat'
import RightChat from './RightChat'

class ChatItem extends Component {
  render() {
    return (
      <View>
        {
          this.props.data.user_id == this.props.userId ?
            <RightChat chat={this.props.data.chat} time={this.props.data.created_at} />
            :
            <LeftChat chat={this.props.data.chat} time={this.props.data.created_at}/>
        }
      </View>
    )
  }
}

export default ChatItem