import React, { Component } from 'react'
import { StyleSheet, View, Text, Image, TextInput, Keyboard, ScrollView, FlatList, TouchableWithoutFeedback } from 'react-native'
import { Navigation } from 'react-native-navigation'
import { Icon } from 'react-native-elements'
import axios from 'axios'
import ChatItem from './ChatItem'
import {config, storageData} from '../../utils'

class Chat extends Component {
  constructor(props) {
    super(props)

    this.state = {
      keyboardToggle: false,
      btnBackHeaderPressed: false,
      sendBtnPressed: false,
      textChat: '',
      test: '',
      room_id: '',
      chatList: [],
      sender: {
        id: 0,
        username: '',
      },
      receiver: {
        id: 0,
        username: '',
      }
    }
  }

  componentDidMount() {
    setInterval(() => this.loadChats(), 500);
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',this._keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',this._keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  chatInputHandler = text => this.setState({textChat: text})

  loadChats = async () => {
    try {
      const token = await storageData.getKey(config.tokenKey)
      const options = {headers: {Authorization : `Bearer ${token}`}}
      const response = await axios.get(`${config.host}/${config.prefix}/rooms/${this.props.roomId}`, options)
      const results = response.data[0]
      this.setState({chatList: results.chats, sender: results.sender, receiver: results.receiver})
    } catch(err) {
      console.error(err)
    }
  }

  sendMessageHandler = async () => {
    if(!(this.state.textChat)) {
      this.setState({
        textChat: '',
        sendBtnPressed: true,
      })
      return null
    }

    try {
      const token = await storageData.getKey(config.tokenKey)
      const options = {headers: {Authorization : `Bearer ${token}`}}
      const payload = {chat: this.state.textChat, room_id: this.props.roomId}
      await axios.post(`${config.host}/${config.prefix}/messages`, payload, options)
    } catch(e) {
      console.error(e)
    }

    this.setState({
      textChat: '',
      sendBtnPressed: true,
    })
  }

  _keyboardDidShow = () => {
    this.setState({keyboardToggle: true});
  }

  _keyboardDidHide = () => {
    this.setState({keyboardToggle: false});
  }

  _goBackHandler = () => {
    this.setState({btnBackHeaderPressed: true})
    setTimeout(() => {
      Navigation.pop(this.props.componentId)
    }, 100)
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.containerHeader}>
          <TouchableWithoutFeedback onPressIn={this._goBackHandler} onPressOut={() => this.setState({btnBackHeaderPressed: false})}>
            <View style={styles.wrapperIconBack}>
              <Icon name="chevron-left" size={30} color={this.state.btnBackHeaderPressed ? "#333" : "#fff"} />
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.containerInfoHeader}>
            <View style={styles.wrapperInfoHeader}>
              <View style={styles.wrapperProfilePhoto}>
                <Image source={require('../../assets/images/anjay.jpg')} style={styles.photoProfile} />
              </View>
              <View style={styles.wrapperUsername}>
                <Text style={styles.username}>
                  {
                    this.state.sender.id === this.props.userId ?
                    this.state.receiver.username : this.state.sender.username
                  }
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{ flex: 2.5 }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <FlatList
              data={this.state.chatList}
              renderItem={({item}) => <ChatItem data={item} userId={this.props.userId} />}
              keyExtractor={(item) => item.id}
            />
          </ScrollView>
        </View>
        <View style={[styles.containerInput, this.state.keyboardToggle ? {flex: 0.5} : {}]}>
          <View style={styles.wrapperInput}>
            <TextInput
              value={this.state.textChat}
              style={styles.textInput}
              placeholder="Type here..."
              onChangeText={this.chatInputHandler}/>
            <View style={styles.sendIcon}>
              <TouchableWithoutFeedback onPressIn={this.sendMessageHandler} onPressOut={() => this.setState({sendBtnPressed: false})}>
                <Icon name="send" type="material-community" color={this.state.sendBtnPressed ? "#333" : "#6746cc"} />
              </TouchableWithoutFeedback>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

export default Chat

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  containerHeader: {
    backgroundColor: '#6746cc',
    flex: 0.7,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#11ece1'
  },
  wrapperIconBack: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: 30
  },
  containerInfoHeader: {
    flex: 1
  },
  wrapperInfoHeader: {
    flexDirection: 'row',
    height: '100%',
    alignItems: 'center'
  },
  wrapperProfilePhoto: {
    height: 50,
    width: 50
  },
  photoProfile: {
    height: 50,
    width: 50,
    borderRadius: 100
  },
  wrapperUsername: {
    flex: 1,
    marginLeft: 10
  },
  username: {
    color: "#f8f8f8",
    fontSize: 25,
    textTransform: 'capitalize'
  },
  containerInput: {
    flex: 0.3,
    padding: 5
  },
  wrapperInput: {
    borderWidth: 1,
    borderColor: '#f6f6f6',
    borderRadius: 50,
    backgroundColor: '#fff',
    flexDirection: 'row'
  },
  textInput: {
    paddingLeft: 20,
    width: '85%'
  },
  sendIcon: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100
  }
})