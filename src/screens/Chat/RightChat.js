import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, TouchableHighlight } from 'react-native'
import Swipeable from 'react-native-swipeable';
import { Icon } from 'react-native-elements'

class RightChat extends Component {

  rightButtons = [
    <TouchableHighlight>
      <View style={{alignItems: 'flex-start', paddingTop: 13}}>
        <Icon size={20} name="delete-forever" type="material-community" color="#333" />
      </View>
    </TouchableHighlight>
  ];

  render() {
    return (
      <View style={styles.wrapperChat}>
          <View style={styles.wrapperTime}>
            <Text style={styles.time}>{this.props.time}</Text>
          </View>
          <Swipeable rightButtons={this.rightButtons}>
            <View style={styles.wrapperBuble}>
              <View style={styles.wrapperChatText}>
                  <Text style={styles.chatText}>{this.props.chat}</Text>
              </View>
            </View>
          </Swipeable>
      </View>
    )
  }
}

export default RightChat

const styles = StyleSheet.create({
  wrapperChat: {
    minHeight: 100,
    marginBottom: 5
  },
  wrapperTime: {
    height: 35,
    justifyContent: 'center',
    alignItems: 'center'
  },
  time: {
    fontSize: 10
  },
  wrapperBuble: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  wrapperProfileImage: {
    flex: 0.2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  profileImage: {
    borderRadius: 100,
    width: 30,
    height: 30
  },
  wrapperChatText: {
    backgroundColor: '#ebebeb',
    maxWidth: 280,
    marginRight: 10,
    borderRadius: 10,
    borderBottomRightRadius: 0
  },
  chatText: {
    padding: 15
  }
})