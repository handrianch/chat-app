import React, { Component } from 'react';
import { View, StyleSheet, Text, TextInput, Button, Alert } from 'react-native';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import {config, storageData} from '../../utils';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      showAlert: false,
      alertTitle: '',
      alertMsg: ''
    };
  }

  componentDidMount() {
    this.tokenChecker()
  }

  _onUsernameChange = text => this.setState({username: text});
  _onPasswordChange = text => this.setState({password: text});

  tokenChecker = async () => {
    let token = await storageData.getKey('id_token')
    if(token) {
      Navigation.setStackRoot(this.props.componentId, [{
          component: {
            name: 'chat.home'
          }
        }]);
    }
  }

  loginHandler = () => {
    if(this.state.username || this.state.password) {
      this.setState({showAlert: false});

      axios.post(`${config.host}/auth`, {
        username: this.state.username,
        password: this.state.password
      })
      .then(response => {
        storageData.saveKey("id_token", response.data.token);
        Navigation.setStackRoot(this.props.componentId, [{
          component: {
            name: 'chat.home'
          }
        }]);
      })
      .catch(err => {
        this.setState({showAlert: true, alertTitle: 'Login Failed', alertMsg: 'Please Check Again Your Credentials'})
      })
    } else {
      this.setState({showAlert: true, alertTitle: 'Form error', alertMsg: 'Please fill all form input'})
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 1 }}>
          <View style={styles.wrapperTitle}>
            <Text style={styles.title}>Welcome</Text>
          </View>
          <View style={{ flex: 1, padding: 10 }}>
            <View style={{ marginBottom: 5 }}>
              <TextInput
                value={this.state.username}
                onChangeText={text => this._onUsernameChange(text)}
                style={styles.textInput}
                placeholder="username" />
            </View>

            <View style={{ marginBottom: 15 }}>
              <TextInput
                value={this.state.pasword}
                onChangeText={text => this._onPasswordChange(text)}
                style={styles.textInput}
                placeholder="password"
                secureTextEntry={true}/>
            </View>

            <View>
              <Button
                title="Come on"
                color="#414a4c"
                onPress={this.loginHandler}
              />
            </View>
          </View>
        </View>

        {
          this.state.showAlert ?
          Alert.alert(this.state.alertTitle, this.state.alertMsg, [{text: 'OK', onPress: () => this.setState({showAlert: false})}])
          :
          <View />
        }

      </View>
    );
  }
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapperTitle: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    flex: 0.5,
    paddingLeft: 20
  },
  title: {
    fontSize: 50,
    textTransform: 'uppercase'
  },
  textInput: {
    borderBottomWidth: 2,
    borderColor: "#414a4c",
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5
  }
})