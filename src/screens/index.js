import { Navigation } from 'react-native-navigation';
import Login from './Login';
import Chat from './Chat';
import Home from './Home';

Navigation.setDefaultOptions({
  topBar: {
    visible: false
  }
});

export default function screensRegister() {
  Navigation.registerComponent("chat.login", () => Login);
  Navigation.registerComponent("chat.room", () => Chat);
  Navigation.registerComponent("chat.home", () => Home);
}