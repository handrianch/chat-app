import { Navigation } from "react-native-navigation";
import screensRegister from './src/screens';

screensRegister();

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      stack: {
        children : [
          {
            component: {
              name: 'chat.login'
            }
          }
        ]
      }
    }
  });
});

